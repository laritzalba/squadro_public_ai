from agent import AlphaBetaAgent
import minimax
import time

"""
Agent skeleton. Fill in the gaps.
"""
class MyAgent(AlphaBetaAgent):

    start_time = None
    time_left= None

    """
    This is the skeleton of an agent to play the Tak game.
    """
    def get_action(self, state, last_action, time_left):
        self.last_action = last_action
        self.time_left = time_left
        self.set_SartTime(time_left)
        #print(self.start_time)
        return minimax.search(state, self)

    """
    The successors function must return (or yield) a list of
    pairs (a, s) in which a is the action played to reach the
    state s.
    """
    def successors(self, state):
        for act in state.get_current_player_actions():
            newState = state.copy()
            if (newState.is_action_valid(act)):
                newState.apply_action(act)
            yield (act, newState)


    """
    The cutoff function returns true if the alpha-beta/minimax
    search has to stop and false otherwise.
    """
    def cutoff(self, state, depth):
        return state.game_over_check() or depth > 3
    """
    The evaluate function must return an integer value
    representing the utility function of the board.
    
    Make a second version of your basic agent which is an exact copy 
    of the first but where the evaluation function takes into account 
    the opponent: it should return the difference between the player’s 
    total advancement and the total advancement of its opponent
    
    """
    def evaluate(self, state):
        sum = 0
        for i in range(0,5):
            sum += state.get_pawn_advancement(self.id, i)
            sum -= state.get_pawn_advancement(1-self.id, i)
        return sum

    """
    get_name on your agent an return the following name: 
    Group XX where XX is your group number. For instance, 
    group 1 will return Group 01 and group 11 will return 
    Group 11.
    """
    def get_name(self):
        return "Group 88_fury"


    """
    Init timer
    """
    def set_SartTime(self, time_left):
        if self.start_time is None:
           self.start_time = time_left
        self.time_left= time_left

    """
      https://stackoverflow.com/questions/3854047/approximate-comparison-in-python
    """
    def approx_Equal(x, y, tolerance=0.2):
        return abs(x - y) <= 0.5 * tolerance * (x + y)