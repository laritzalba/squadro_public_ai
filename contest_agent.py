from agent import AlphaBetaAgent
import minimax
from time import time

"""
Agent skeleton. Fill in the gaps.
"""
class MyAgent(AlphaBetaAgent):

    iterDepth = 1

    """
    This is the skeleton of an agent to play the Tak game.
    """
    def get_action(self, state, last_action, time_left):
        time_to_compute = time_left/10
        self.iterDepth = 1
        timer_start = time()
        timer_now = time()
        best_action = None
        while timer_now - timer_start < time_to_compute :
            print(timer_now - timer_start)
            best_action = minimax.search(state, self)
            self.iterDepth += 1
            timer_now = time()
        return best_action

    """
    The successors function must return (or yield) a list of
    pairs (a, s) in which a is the action played to reach the
    state s.
    """
    def successors(self, state):
        for act in state.get_current_player_actions():
            newState = state.copy()
            if (newState.is_action_valid(act)):
                newState.apply_action(act)
            yield (act, newState)


    """
    The cutoff function returns true if the alpha-beta/minimax
    search has to stop and false otherwise.
    """
    def cutoff(self, state, depth):
        return state.game_over_check() or depth > self.iterDepth
    """
    The evaluate function must return an integer value
    representing the utility function of the board.
    
    Make a second version of your basic agent which is an exact copy 
    of the first but where the evaluation function takes into account 
    the opponent: it should return the difference between the player’s 
    total advancement and the total advancement of its opponent
    
    """

    def evaluate(self, state):
        sum = 0
        for i in range(0, 5):
            sum += state.get_pawn_advancement(self.id, i)
            sum -= state.get_pawn_advancement(1 - self.id, i)

        if state.cur_player == state.get_winner():  # si je suis gagnante
            sum += 10000
        elif 1 - state.cur_player == state.get_winner():
            sum -= 10000
        return sum

    """
    get_name on your agent an return the following name: 
    Group XX where XX is your group number. For instance, 
    group 1 will return Group 01 and group 11 will return 
    Group 11.
    """
    def get_name(self):
        return "Group 88"
